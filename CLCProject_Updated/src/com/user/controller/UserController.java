package com.user.controller;



import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.login.module.LoginModuleInterface;
import com.user.model.User;

@Controller
@RequestMapping("/user")
public class UserController {
	
	LoginModuleInterface loginService;

	
		@RequestMapping(path = "/login", method = RequestMethod.GET)
		
		public ModelAndView displayForm() {
			
					
			return new ModelAndView("login","user", new User());
		}
		
		@RequestMapping(path = "/registration", method = RequestMethod.POST)
		public ModelAndView loginUser(@Valid @ModelAttribute("user") User user, BindingResult result, Errors errors, Model model)
		{
		
			// If invalid data is entered, returns to addUser page
			if (errors.hasErrors()) {
			
		        return new ModelAndView("login");
		    }

			 model.addAttribute("user", user);
			 
			 
			return new ModelAndView("display");
		
		}



		public void setLoginService(LoginModuleInterface loginService) {
			this.loginService = loginService;
		}



}	
		
	