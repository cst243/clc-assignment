<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix= "tiles" %>
<%@ page import="java.io.*,java.util.*" language="java" contentType="text/html;charset=ISO-8859-1"pageEncoding="ISO-8859-1"%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link rel="stylesheet" type="text/css" href="styles.css">
	<title>CST-341 CLC Project</title>
	
</head>

<body>
	<!-- Header -->
	<tiles:insertAttribute name="header" />

	<!-- Body Page -->
	<div align="center">
		<tiles:insertAttribute name="body" />
	</div>

	<!-- Footer Page -->
	<tiles:insertAttribute name="footer" />
</body>

</html>